package jp.fedom.android.oauth.test.sample;

import jp.fedom.android.oauth.SampleClass;
import junit.framework.TestCase;

public final class SampleTest extends TestCase {

    public SampleTest(final String name) {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void test_Sample() {
        assertEquals(1,SampleClass.SampleMethod());
    }
}
